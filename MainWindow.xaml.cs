﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TelegramWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void GrayColor(object sender, RoutedEventArgs e)
        {
            if (qwe.Background == Brushes.White)
            {
                qwe.Background = Brushes.Gray;
            }
            else if (qwe.Background == Brushes.MediumVioletRed)
            {
                qwe.Background = Brushes.Gray;
            }
        }

        private void VioletColor(object sender, RoutedEventArgs e)
        {
            if (qwe.Background == Brushes.White)
            {
                qwe.Background = Brushes.MediumVioletRed;
            }
            else if (qwe.Background == Brushes.Gray)
            {
                qwe.Background = Brushes.MediumVioletRed;
            }

        }

        private void WhiteColor(object sender, RoutedEventArgs e)
        {
            if (qwe.Background == Brushes.MediumVioletRed)
            {
                qwe.Background = Brushes.White;
            }
            else if (qwe.Background == Brushes.Gray)
            {
                qwe.Background = Brushes.White;
            }
        }
    }
}
